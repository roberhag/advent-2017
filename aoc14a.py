inst = "xlqgujun"
from knothash import knothash

us = 0
for i in range(128):
    #print(i) #Progress tracker
    kh = knothash(inst+"-%i"%i)
    nu = str(bin(eval('0x'+kh)))[2:]
    pad = 128 - len(nu)
    nu = '0'*pad + nu
    for c in nu:
        us += int(c)
print(us)
