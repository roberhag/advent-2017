from collections import defaultdict
lines = open('inp08.txt', 'r')
inst = lines.readlines()

regs = defaultdict(lambda:0)

i = 0
while i < len(inst):
    do, cond = inst[i].split('if')
    a, c, b = cond.split() #hopefully this is always three things
    if ((c == '<') and (regs[a] < int(b))) or ((c == '>') and (regs[a] > int(b))) or \
    ((c == '<=') and (regs[a] <= int(b))) or ((c == '>=') and (regs[a] >= int(b))) or \
    ((c == '==') and (regs[a] == int(b))) or ((c == '!=') and (regs[a] != int(b))):
        a, c, b = do.split()
        if c == 'inc':
            regs[a] += int(b)
        elif c == 'dec':
            regs[a] -= int(b)
        else:
            print("what is", c)
    i += 1

print(max(regs.values()))
