import string

word = "OTUJNMQTYOQOVVNEOXQVAOXJEYA"
#word = "PWVAYOBB"

alph = string.ascii_uppercase
roted = []
for a in alph:
    rot = (2*ord(a) - 64) #how far to rotate
    num = 65 + (3*ord(a) - 129)%26 #Make rotation periodical, (subtract and add 65 for rotation A to go through 0)
    roted.append(chr(num))

roted = ''.join(roted)

trans = str.maketrans(roted, alph)
out = str.translate(word, trans)   #Nice built-in function

print(out)
