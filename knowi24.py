from numpy import *

ports = []
N = 10000

with open("portals.txt") as infile:
    for line in infile:
        ports.append([eval(x) for x in line.split('->')])

grid = zeros((N+1,N+1),dtype=uint16)

grid[:,:] = 2*N

todo = [(N-1, N-1)]

i = 1

while grid[0,0] == 2*N:
    print (i, len(todo))
    #first find all of dist i
    current = todo #for this i
    todo = []      #for next i
    #then find neighbors for each
    if len(current) == 0:
        print("something wrong!")
        exit(10)
    for coord in current:
        x,y = coord
        grid[x,y] = i-1 #especially for first
        
        for xx, yy in ((x,y+1), (x,y-1), (x+1,y), (x-1,y)):
            if xx > N or xx < 0 or yy > N or yy < 0:
                continue
            for portal in ports: #check if portal there
                if portal[1] == (xx,yy):
                    xx, yy = portal[0]
                    break
            if grid[xx,yy] <= i:
                continue
            grid[xx,yy] = i
            todo.append((xx,yy))
                
    i += 1

moves = []
print(grid[0,0])
pos = (0,0)
for j in range(i-2,0,-1):
    #print(j)
    x, y = pos
    di = False
    #dirs = ['N', 'O', 'H', 'V']
    for k in range(2):
        for xx, yy, d in [[x, y+1, 'N'], [x, y-1, 'O'], [x+1, y, 'H'], [x-1, y, 'V']]:
            if xx > N or xx < 0 or yy > N or yy < 0:
                continue
            if grid[xx,yy] == j:
                print(d, end="")
                di = True
                break
        else: #there must be a portal here
            for portal in ports: #check if portal there
                    if portal[0] == (x,y):
                        x, y = portal[1]
                        break
            else:
                print("no portal at", pos)
        if di:
            break
    pos = (xx, yy)
    
