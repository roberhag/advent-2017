from numpy import *


inf = open("ikno04.txt")

i = 0
for line in inf:
    w = line.strip()
    if w[::-1] == w:
        continue #allerede palindrom

    w = list(sorted(w))
    w.append('yy')
    #print(w)
    works = True
    pl = 'xx'
    hasodd = False
    cnt = 0
    for l in w:
        if l == pl:
            cnt += 1
        else:
            #print(cnt)
            if(cnt%2 == 1): #not even count
                if hasodd:
                    works = False
                    break
                else:
                    hasodd = True
            cnt = 1 #this is the first letter of next kind
        pl = l
    if works:
        i += 1
        print(i, line.strip())
