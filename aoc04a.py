from numpy import *


inf = open("inp04.txt")

i = 0
for line in inf:
    words = line.split()
    if len(words) == 0:
        continue
    swords = sorted(words)
    i += 1
    for j in range(1,len(swords)):
        if swords[j] == swords[j-1]:
            i -= 1
            break
print(i)
