from numpy import *

spos = None
with open('inp19.txt', 'r') as infile:
    grid = []
    for line in infile:
        gline = [c for c in line]
        if not spos:
            spos = gline.index('|') # First line, find starting pos
        gline.append(' ')           # padding along the edge
        grid.append(gline)
        #print(''.join(gline))      # For test printing

carr = array(grid, dtype=string_)
carr = carr.transpose()

#print(carr.shape)
wt = len(carr[0]) - 1 #minus padding

pos = array((spos,0))
dirs = array(((1,0),(0,1),(-1,0),(0,-1)))

di = 1
d = dirs[1] #going south first
visited = []

i = 1
while (pos >= 0).all() and (pos < wt).all():
    pos += d                                    # Move
    c = carr[pos[0],pos[1]]                     # current character
    if c == b'+':                               # intersection
        ok = False
        if di in (1,3):                         #switch from n/s to e/w
            for pdi in (0,2):
                pd = pos+dirs[pdi]
                if carr[pd[0], pd[1]] == b'-':  #found correct direction
                    di = pdi
                    ok = True
        elif di in (0,2):                       #switch from e/w to n/s
            for pdi in (1,3):
                pd = pos+dirs[pdi]
                if carr[pd[0], pd[1]] == b'|':
                    di = pdi
                    ok = True
        if not ok:
            print("numbawang")                  # failsafe. Should not happen
        d = dirs[di]
    i += 1
    if (ord(c) in range(65,91)) or (ord(c) in range(97,122)): #a-z, A-Z
        visited.append(c.decode())              # from numpy bytes to str
        print(c)
        if c == b'H':   # NB! This END POINT is hardcoded for my specific input
            print(i)    # 
            break
    if (i > 2*wt*wt):   # passed at least some squares more than twice,
        break           # We are in some vicious loop
print(''.join(visited))
