from numpy import *
from sys import exit
regs = {}
for r in 'abcdefgh':
    regs[r] = 0

def getval(val):
    try:
        return int(val)
    except:
        return regs[val]
muls = 0
with open("inp23.txt") as infile:
    i = 0
    lines = infile.readlines()
    N = len(lines)
    while i < N and i >= 0:
        coms = lines[i].split()
        #print(i, coms)
        if coms[0] == 'snd':
            snd = getval(coms[1])
            #print(snd)
        elif coms[0] == 'set':
            regs[coms[1]] = getval(coms[2])
        elif coms[0] == 'sub':
            regs[coms[1]] -= getval(coms[2])
        elif coms[0] == 'add':
            regs[coms[1]] += getval(coms[2])
        elif coms[0] == 'mul':
            regs[coms[1]] *= getval(coms[2])
            muls += 1
        elif coms[0] == 'mod':
            regs[coms[1]] %= getval(coms[2])
        elif coms[0] == 'jnz':
            if getval(coms[1]) != 0:
                i += getval(coms[2]) - 1 #account for +1 later
        elif coms[0] == 'jgz':
            if getval(coms[1]) > 0:
                i += getval(coms[2]) - 1 #account for +1 later
        elif coms[0] == 'rcv':
            if getval(coms[1]) != 0:
                print("recovered", snd)
                exit()
        else:
            print("Not implemented", coms)
        
        i += 1
        
    
print(muls)

