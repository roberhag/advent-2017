from numpy import *
from sys import exit
regs = {}
for r in 'abcdefgh':
    regs[r] = 0
regs['a'] = 1 # non-debug register

def getval(val):
    try:
        return int(val)
    except:
        return regs[val]

with open("inp23o.txt") as infile: #using optimized infile
    i = 0
    lines = infile.readlines()
    N = len(lines)
    ii = 0
    while i < N and i >= 0:
        coms = lines[i].split()
        if ii % 1000000 == 0:
            print(i, regs)
        ii += 1
        #print(i, coms)
        if coms[0] == 'snd':
            snd = getval(coms[1])
            #print(snd)
        elif coms[0] == 'set':
            regs[coms[1]] = getval(coms[2])
        elif coms[0] == 'sub':
            regs[coms[1]] -= getval(coms[2])
        elif coms[0] == 'add':
            regs[coms[1]] += getval(coms[2])
        elif coms[0] == 'mul':
            regs[coms[1]] *= getval(coms[2])
        elif coms[0] == 'mod':
            regs[coms[1]] %= getval(coms[2])
        elif coms[0] == 'jnz':
            if getval(coms[1]) != 0:
                i += getval(coms[2]) - 1 #account for +1 later
        elif coms[0] == 'jgz':
            if getval(coms[1]) > 0:
                i += getval(coms[2]) - 1 #account for +1 later
        elif coms[0] == 'rcv':
            if getval(coms[1]) != 0:
                print("recovered", snd)
                exit()
        else:
            print("Not implemented", coms)
        
        i += 1
        
    
print(regs['h'])

