#from numpy import *
from sys import exit
#regs = {}

with open("inp24.txt") as infile: #using optimized infile
    comps = []
    starters = []
    i = 0
    for line in infile:
        comps.append([int(x) for x in line.split('/')])
        if 0 in comps[-1]:
            starters.append(i)
        i += 1
            
totry = {}
totri = []
for s in starters:
    totry[s] = [0] #pin connected
    totri.append(s)

smax = 0
lmax = 0

while len(totri) > 0:
    itm = totri.pop()
    curlist = totry.pop(itm)
    itm %= 1000 #some duplicates, separated by 1000s
    curlist.append(itm)
    a, b = comps[itm] #current component
    endpins = curlist[0]
    if a == endpins:
        curlist[0] = b
    elif b == endpins:
        curlist[0] = a
    endpins = curlist[0] #endpins updated
    
    added = 0 #how many new possibilities
    for i in range(len(comps)):
        if i in curlist[1:]: #first is just endpin number
            continue
        if not endpins in comps[i]:
            continue #not compatible with this bridge
        while i in totri: #there is a possible bridge using same index
            i += 1000     #to distinguish, use index + 1000
        totri.append(i) 
        totry[i] = curlist.copy() #the totry dict is extended
        
        added += 1
    if added == 0: #no more compatible components, bridge done 
        if len(curlist) >= lmax:
            suma = 0
            for ind in curlist[1:]:
                comp = comps[ind]
                suma += sum(comp)
                #print(comp, end=' ')
            if lmax == len(curlist):
                smax = max(smax, suma)
            else:
                smax = suma
            lmax = len(curlist)
            print(lmax, suma)
    
    
print(smax)

