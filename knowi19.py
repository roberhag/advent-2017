from numpy import *
import matplotlib.pyplot as plt

#grid = zeros((400, 400), dtype = uint8)

pos = [(0,0)]
with open('iknow19.txt', 'r') as infile:
    for line in infile:
        ln, di = line.split(', ')
        ln = int(ln) #length
        p = pos[-1]
        if di[0] == 'n':
            p2 = (p[0], p[1]+ln)
        elif di[0] == 's':
            p2 = (p[0], p[1]-ln)
        elif di[0] == 'e':
            p2 = (p[0]+ln, p[1])
        elif di[0] == 'w':
            p2 = (p[0]-ln, p[1])
        pos.append(p2)

pos = array(pos)

plt.plot(pos[:,0],pos[:,1], linewidth=12)

plt.show()
