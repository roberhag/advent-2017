from numpy import *

i = 0
message = "1110010101000001011000000011101110100101010011011010101101100000010001111101000001010010001011101001100100100011010000110101111101010011100010110001100111110010"

alfab = "AÁBDÐEÉFGHIÍJKLMNOÓPRSTUÚVXYÝÞÆÖ" #Luckily, python 3 supports unicode natively
counts = zeros(32, dtype = uint)
with open("iknow18.txt") as innfil:
    for c in innfil.read():
        if c in alfab:
            i = alfab.index(c) #count occurence of the characters in alfab
            counts[i] += 1


key = ''
sr = sorted(counts)[::-1] #decreasing order

for n in sr:
    i = int(where(counts == n)[0])
    st = bin(i)[2:]
    st = (5-len(st))*'0' + st #each character represents 5 bit numbers from 0 to 31
    key = key + st

bmes = int('0b' + message,2)
bkey = int('0b' + key,2)

dec = bmes ^ bkey #Luckily, python can handle XORing arbitrarily large integers.

print(dec)
sdec = '0' + bin(dec)[2:] #leading 0 because we want a multiple of 8 bits
print(sdec)
for i in range(0,len(sdec),8):
    bchar = int('0b' + sdec[i:i+8],2)
    print(chr(bchar),end='')
#This could be done in a better way by mod-ing the integer by powers of 256
print()
