from numpy import *
from collections import defaultdict
depths = defaultdict(lambda:0)

with open("inp13.txt") as infile:
    for line in infile:
        p, d = line.split(': ')
        depths[int(p)] = int(d)

ds = zeros(max(depths.keys()) + 1, dtype=uint)
pos = arange(max(depths.keys()) + 1, dtype=uint)

ds = array(depths.values(), dtype=uint)
pos = array(depths.keys(), dtype=uint)

ds = (2*(ds-1))

i = 0
while (((pos + i) % ds) == 0).any(): #for some reason this is still slow-ish!
    i += 1
    
print(i)

