#from numpy import *
from sys import exit
#regs = {}

with open("inp24.txt") as infile: #using optimized infile
    comps = []
    starters = []
    i = 0
    for line in infile:
        comps.append([int(x) for x in line.split('/')])
        if 0 in comps[-1]:
            starters.append(i)
        i += 1
            
totry = {}
totri = []
for s in starters:
    totry[s] = [0] #pin connected
    totri.append(s)

smax = 0

while len(totry) > 0:
    try:
        itm = totri.pop()
        curlist = totry.pop(itm)
    except:
        print(totri)
        print(totry)
        input()
    itm %= 1000 #some duplicates
    curlist.append(itm)
    a, b = comps[itm] #current component
    endpins = curlist[0]
    if a == endpins:
        curlist[0] = b
    elif b == endpins:
        curlist[0] = a
    endpins = curlist[0] #endpins updated
    
    added = 0 #how many new possibilities
    for i in range(len(comps)):
        if i in curlist[1:]: #first is just endpin number
            continue
        if not endpins in comps[i]:
            continue #not compatible with this bridge
        while i in totri:
            i += 1000
        totri.append(i) #TODO when adding an index already in list totri,
        totry[i] = curlist.copy() #the totry dict is not extended, but the key modified!
        
        added += 1
    if added == 0: #no more compatible components
        suma = 0
        for ind in curlist[1:]:
            comp = comps[ind]
            suma += sum(comp)
            #print(comp, end=' ')
        smax = max(smax, suma)
        #print(suma)
    
    
print(smax)

