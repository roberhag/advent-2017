from numpy import *
from itertools import cycle

N = 1000 #hardcoded num parts

pos = zeros((N,3), dtype = int64)
vel = zeros((N,3), dtype = int64)
acc = zeros((N,3), dtype = int64)

arrays = cycle([pos, vel, acc])

with open("inp20.txt") as infile:
    i = 0
    for line in infile:
        pva = line.strip().split(', ')
        for a in pva:
            a = a[3:-1]
            arr = next(arrays)
            arr[i] = [int(k) for k in a.split(',')]
        i += 1
        
alive = ones(N, dtype=bool)
i = 0

while True:
    pi = argsort(pos[:,0]) #sort along x-axis to speed up the O(N^2) problem
    for j in range(N-1):
        if not alive[pi[j]]:
            continue
            
        for k in range(j+1, N):
            if not alive[pi[k]]:
                continue
            if pos[pi[j],0] != pos[pi[k],0]: #not at same x-coord anymore
                break
            if (pos[pi[j],1] == pos[pi[k],1]) and (pos[pi[j],2] == pos[pi[k],2]):
                alive[pi[j]] = False
                alive[pi[k]] = False
                #print('KILL')
    if i%1000 == 0:
        k = sum(alive)
        print("Alive %3i   Time %i" % (k, i/1000))
        if i > 5000:
            print('Probably safe to ctrl-C now...')

    vel[:,:] += acc[:,:]
    pos[:,:] += vel[:,:]
    i += 1
