from numpy import *

afac = 16807
bfac = 48271

div = 2147483647

A = 883
B = 879

#A = 65
#B = 8921

#with open("inp15.txt") as infile:
#    for line in infile:
#        p, d = line.split(': ')

match = 0

for i in range(5000000):
    if i%100000 == 0: print(i/1e6)
    aok = 1
    while aok:
        A *= afac
        A %= div
        av = A%65536
        aok = av%4
    
    bok = 1
    while bok:
        B *= bfac
        B %= div
        bv = B%65536
        bok = bv%8
    
    match += (av==bv)
    

print(match)
