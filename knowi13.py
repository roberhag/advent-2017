from numpy import *

vals = []

with open("ikno13.txt") as infile:
    for line in infile:
        p, d = line.split(', ')
        vals.append(int(d))

vals = sorted(vals)[::-1]

drag = zeros(len(vals))

drag[0] = -1 #A
drag[1] = 1  #B
i = 2
sumbob = vals[1]

while i < len(vals):
    n = int(log(i)/log(2))
    for j in range(2**n):
        k = i+j
        if k == len(vals):
            break
        drag[k] = -drag[j]
        if drag[k] == 1: #Bob
            sumbob += vals[k]
    i += 2**n
    
print(sumbob)
