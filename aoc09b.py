import re, sys
lines = open('inp09.txt', 'r')
inst = lines.read().strip()

#binst = inst #copy

binst = re.sub(r'!.','', inst) #ignoring next char

def rep(garb):
    return 'x'*(len(garb.group())-2)


binst = re.sub(r'<.*?>',rep, binst) #

print(binst.count('x'))
