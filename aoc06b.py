from numpy import *

lines = open("inp06.txt")

bank = [int(a) for a in lines.readlines()[0].split()]

l = len(bank)

confs = [] #list in which to save bank configurations
cycs = 0   #cycle counter

while bank not in confs:
    confs.append(bank.copy()) #add bank config to list
    i = argmax(bank)
    redi = bank[i]            #how many to redistribute
    bank[i] = 0
    #there has to be a smarter way, probably one can redistribute multiple at a time instead of one.
    #However, this algorithm works, also it seems the numbers are not significantly bigger than l.
    for j in range(redi): #start redistribution .. one by one
        k = (i + j + 1)%l #circular
        bank[k] += 1
    cycs += 1

print(len(confs) - confs.index(bank)) #the amount of cycles between this and start of loop
