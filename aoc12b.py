infi = open("inp12.txt")
lis = []
for line in infi:
    n, cons = line.split('<->')
    cons = cons.split(',')
    lislis = [int(con.strip()) for con in cons]
    if len(line) < 1:
        continue
    lis.append(lislis)

def ingroup(sw):
    visited = []
    checknext =[sw]
    while len(checknext)>0:
        check = checknext.pop()
        visited.append(check)
        for l in lis[check]:
            if l not in visited + checknext:
                checknext.append(l)
    return visited

groups = 0
v = 0
totvis = []

while v < len(lis):
    print(v)
    vis = ingroup(v)
    totvis += vis
    groups += 1
    while v in totvis:
        v += 1 #now v not visited
 
print(groups)

