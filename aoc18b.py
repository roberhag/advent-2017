from numpy import *
from sys import exit
from time import sleep
from threading import Thread
from collections import defaultdict
regs0 = defaultdict(lambda:0)
regs1 = defaultdict(lambda:0)
regs1['p'] = 1
regs = [regs0, regs1]
queue= [[],[]]
sleeping = False
exited = False

def getval(val, pp):
    try:
        return int(val)
    except:
        if pp == 0:
            return regs0[val]
        else:
            return regs1[val]

sendcn = 0

def run(pp):
    global queue, sleeping, exited, sendcn
    with open("inp18.txt") as infile:
        i = 0
        lines = infile.readlines()
        N = len(lines)
        while i < N and i >= 0:
            coms = lines[i].split()
            #print(i, coms)
            if coms[0] == 'snd':
                queue[1-pp].append( getval(coms[1], pp) )
                if pp:
                    sendcn += 1
            elif coms[0] == 'set':
                regs[pp][coms[1]] = getval(coms[2], pp)
            elif coms[0] == 'add':
                regs[pp][coms[1]] += getval(coms[2], pp)
            elif coms[0] == 'mul':
                regs[pp][coms[1]] *= getval(coms[2], pp)
            elif coms[0] == 'mod':
                regs[pp][coms[1]] %= getval(coms[2], pp)
            elif coms[0] == 'jgz':
                if getval(coms[1], pp) > 0:
                    i += getval(coms[2], pp) - 1 #account for +1 later
            elif coms[0] == 'rcv':
                sc = 0
                while len(queue[pp]) == 0:
                    if sc > 100:
                        print(pp, sc)
                        if pp:
                            print(sendcn)
                        exit()
                        if sleeping: #This test did not work as expected?
                            print("Deadlock")
                            exited = True
                            if pp:
                                print(sendcn)
                            exit()
                        if exited:
                            print("other tread is dead")
                            exited = True
                            if pp:
                                print(sendcn)
                            exit()
                    sc += 1 #sleep counter
                    sleeping = True
                    sleep(0.01)
                    sleeping = False
                regs[pp][coms[1]] = queue[pp].pop(0)
            else:
                print("Not implemented", coms)
            
            i += 1
            
        
    print("done", pp)
    if pp:
        print(sendcn)

thread1 = Thread( target=run, args=[0] )
thread2 = Thread( target=run, args=[1] )

thread1.start()
thread2.start()
thread1.join()
thread2.join()
