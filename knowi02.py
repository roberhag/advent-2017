import numpy as np
N = 1000
laby = np.zeros([N,N], dtype = np.uint64)

#x, y = np.meshgrid(np.arange(1,N+1,dtype=np.uint64),np.arange(1,N+1,dtype=np.uint64), copy=False)

#laby[:,:] = x**3 + 12*x*y + 5*x*y**2

"""
for x in range(1,N+1):
    print(x)
    for y in range(1,N+1):
        nu = x**3 + 12*x*y + 5*x*y**2
        laby[x-1,y-1] = sum([int(a) for a in str(bin(nu))[2:]]) % 2 #0 hvis par, 1 hvis odde

np.save("walls02.npy", laby)
print("saved!") """
laby = np.load("walls02.npy")

laby[0,0] = 2


print(np.sum(laby.flatten()==0))

#i = 0
while((laby==2).any()):
    r,c = np.where(laby==2)
    r = r[0]
    c = c[0]
    for x,y in [(r-1, c), (r+1,c), (r, c-1), (r,c+1)]:
        if x<0 or x >=N:
            continue
        if y<0 or y >=N:
            continue
        if laby[x,y] == 0:
            laby[x,y] = 2
    laby[r,c] = 3 #sett inn en vegg her, for her har vi vaert
    #print(laby[:10,:10])
    #input()

#alle steder vi ikke kan besoeke er 0
print(laby[:10,:10])

print(np.sum(laby.flatten()==0))


