from numpy import *
from knothash import knothash

inst = "xlqgujun"

grid = zeros([128,128], dtype=int)
for i in range(128):
    #print(i) #print progress
    kh = knothash(inst+"-%i"%i)
    nu = str(bin(eval('0x'+kh)))[2:]
    pad = 128 - len(nu)
    nu = '0'*pad + nu
    for j in range(128):
        grid[i,j] = -int(nu[j])
        
#print(grid[:8,:8]) 

g = 0

#BFS
for i in range(128):
    print(i)
    for j in range(128):
        if grid[i,j] == -1:
            tovisit = [(i,j)] #a stack of coordinates in this group
            g += 1
            while len(tovisit) > 0:
                ii,jj = tovisit.pop()
                grid[ii,jj] = g
                for x, y in ((ii,jj+1),(ii,jj-1),(ii+1,jj),(ii-1,jj)):
                    if x < 0 or x > 127 or y < 0 or y > 127:
                        continue
                    if grid[x,y] == -1:
                        tovisit.append((x,y))
                        grid[x,y] = g

print(g)
