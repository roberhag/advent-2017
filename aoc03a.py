from numpy import *
from itertools import cycle

square = 289326

coord = array((0,0))
dirs = cycle((array((1,0)),array((0,1)),array((-1,0)),array((0,-1))))
di = next(dirs)
r = 0     #starting values for initial coordinate
diam2 = 1
for i in range(1, square):
    diam = diam2 #square to be filled before going to next layer
    coord += di #test move
    r = max(abs(coord))
    diam2 = int(2*r + 1) #square to be filled if at next coord
    if (i < int(diam**2)) and (diam2 > diam): #if square not filled, but test move outside,
        coord -= di    #move back
        di = next(dirs)
        coord += di
        r = max(abs(coord))
        diam2 = int(2*r + 1) #update square to be filled next iteration
        
    #print(i+1, coord, r)


print(i+1, coord)
print(sum(abs(coord))) #taxicab geometry
