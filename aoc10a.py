lines = open("inp10.txt")

inst = lines.read().strip().split(',')

lis = 2*list(range(256)) #ghost points, for wrapping

i = 0
skip = 0

for lest in inst:
    length = int(lest)
    lis[i:i+length] = lis[i:i+length][::-1] #pinch and twist
    lis[0:i] = lis[256:256+i] #copy ghost points back to main list
    lis[256:]=lis[:256]       #copy main list completely to the ghost list
    i += length + skip
    i %= 256
    skip += 1

print(lis[0]*lis[1])
