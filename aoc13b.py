#from numpy import *
from collections import defaultdict
depths = defaultdict(lambda:None)

with open("inp13.txt") as infile:
    for line in infile:
        p, d = line.split(': ')
        depths[int(p)] = int(d)



sever = 0
starti = 0
ok = False
#Tractor method, was good enough!
while not ok:
    ok = True
    starti += 1 #delayed start
    i = starti
    d = 0
    while d <= max(depths.keys()): #d is our depth, i is the time
        r = depths[d]
        if r: #there is a wall here
            p = i % (2*(r-1))
            if p == 0:
                ok = False
                break
        d += 1
        i += 1
    
print(starti)

