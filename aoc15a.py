from numpy import *

afac = 16807
bfac = 48271

div = 2147483647

A = 883
B = 879

#A = 65
#B = 8921

#with open("inp15.txt") as infile:
#    for line in infile:
#        p, d = line.split(': ')

match = 0

for i in range(40000000):
    if i%10000 == 0: print(i)
    A *= afac
    B *= bfac
    A %= div
    B %= div
    abi = str(bin(A))[-16:]
    bbi = str(bin(B))[-16:]
    
    match += (abi == bbi)
    

print(match)
