from numpy import *
import string

def hexdist(x,y):
    #TODO

pos = zeros(2)
steps = zeros([6,2])
theta = pi/2
for i in range(6): #n, nw, sw, s, se, ne
    steps[i] = [cos(theta), sin(theta)]
    theta += pi/3
#print(steps)
madist = 0
with open("inp11.txt") as infile:
    dirs = infile.read().strip()
    dirs = string.replace(dirs,"nw","1")
    dirs = string.replace(dirs,"sw","2")
    dirs = string.replace(dirs,"se","4")
    dirs = string.replace(dirs,"ne","5")
    dirs = string.replace(dirs,"n","0")
    dirs = string.replace(dirs,"s","3")
    dirs = dirs.split(",")
    for di in dirs:
        di = int(di)
        pos += steps[di]
        mapos = pos.copy()
        if (pos[0]<-700) and (pos[1]<-800):
            dist = 0
            while mapos[0] < -0.1: #some tolerance
                dist += 1
                mapos += steps[5]

            while mapos[1] < -0.1: #some tolerance
                dist += 1
                mapos += steps[0]
            if dist > madist:
                madist = dist

print(madist)
