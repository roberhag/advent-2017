lines = open('ikno01.txt', 'r')
anag = "aeteesasrsssstaesersrrsse"
lets = set(anag)
anlen = len(anag)

ansor = ''.join(sorted(anag))

n = 1
lenn = anlen
while n < 26:
    n += 1
    lenn = anlen/n + n - 1
    if int(lenn) == lenn: break
print (n, lenn) #dette viser at n = 5 og lengden av originalt ord er 9.

def ngram(stri, n):
    out = ""
    for i in range(len(stri) - n + 1):
        out += stri[i:i+n]
    return out

for line in lines:
    line = line.strip()
    if len(line) == lenn: #word has correct length
        lins = set(line)
        if lins == lets: #here is a possibility, they have same letters
            check = ''.join(sorted(ngram(line, n)))
            if check == ansor:
                print("%i-%s" % (n,line))
print("done")

