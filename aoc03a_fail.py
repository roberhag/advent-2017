from numpy import *

square = 289326

diam = sqrt(square)
print(int(diam)) #odd number, so this is the size of the square of grids within

r = int(diam/2)
print(r)

lefto = square - int(diam)**2 #leftover
print(lefto)
lefto -= int(diam) #move along one side
print(lefto)

d = r +1+ abs((r+1)-lefto) #find coordinate along next side
print(d) #one of the +1 in the above line is wrong :(
