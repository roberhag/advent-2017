from numpy import *
from itertools import cycle

square = 289326
rm = 300 #hope this is big enough radius
board = zeros((2*rm,2*rm)) #I need to save in array now
#board[0,0] = 1

coord = array((0,0))

cx = coord[0] + rm
cy = coord[1] + rm

board[cx,cy] = 1

print(board[cx-1:cx+2, cy-1:cy+2])

dirs = cycle((array((1,0)),array((0,1)),array((-1,0)),array((0,-1))))
di = next(dirs)
r = 0
diam2 = 1
for i in range(1, square):
    diam = diam2
    coord += di
    r = max(abs(coord))
    diam2 = int(2*r + 1) #to be filled
    if (i < int(diam**2)) and (diam2 > diam):
        coord -= di
        di = next(dirs)
        coord += di
        r = max(abs(coord))
        diam2 = int(2*r + 1) #to be filled
    #now we have coordinate, sum around
    cx = coord[0] + rm
    cy = coord[1] + rm
    susu = sum(board[cx-1:cx+2, cy-1:cy+2])
    if susu >square:
        print(susu)
        break
    board[cx,cy] = susu

