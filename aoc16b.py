progs = "abcdefghijklmnop"
progs = [c for c in progs]
np = len(progs)

with open("inp16.txt") as infile:
    textfile = infile.read()
    #textfile = "s1,x3/4,pe/b"
    onedance = textfile.split(',')
    dancelist = []
    for line in onedance:
        c = line[0]
        if c == 's': #spin
            n = int(line[1:])
            dancelist.append([0,n])
        else:
            if c == 'x': #exchange
                a,b = [int(i) for i in line[1:].split('/')]
                dancelist.append([1,a,b])
            else: #Partner
                a,b = line[1:].split('/')
                dancelist.append([2,a,b])

    for ddd in range(1000000000):
        if ddd%1000000 == 0:
            print(ddd/1000000)
            
        for actn in dancelist:
            c = actn[0]
            if c == 0: #spin
                n = actn[1]
                progs2 = progs[:-n]
                progs = progs[-n:] + progs2
            else:
                a,b = actn[1:]
                if c == 1: #exchange
                    pp = progs[b]
                    progs2[b] = progs[a]
                    progs2[a] = pp
                else: #Partner
                    i = progs2.index(a)
                    j = progs2.index(b)
                    progs2[i] = b
                    progs2[j] = a
            
        
print()
sever = ''.join(progs)
print(sever)
