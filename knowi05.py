from numpy import *

arr = zeros(1000001, dtype = uint) #Bruker ikke index 0, så en ekstra.

arr[1] = 1
arr[2] = 2 #fyller de to første så jeg slipper spesialtilfelle
N = len(arr)

cnt = 1
i = 2

while i < N:
    cnt += 1                # Tallet som skal fylles inn i arrayen
    n = arr[cnt]            # antall ganger tallet skal fylles
    if i+n >= N:            # Spesialtilfelle for å ikke skrive over enden av arr
        arr[i:] = cnt
    else:
        arr[i:i+n] = cnt    # Tilføy tallet n ganger i arr
    i += n                  # inkrementer index

print(sum(arr))
