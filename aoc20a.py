from numpy import *
from itertools import cycle

N = 1000 #hardcoded num parts

pos = zeros((N,3), dtype = int64)
vel = zeros((N,3), dtype = int64)
acc = zeros((N,3), dtype = int64)

arrays = cycle([pos, vel, acc])

with open("inp20.txt") as infile:
    i = 0
    for line in infile:
        pva = line.strip().split(', ')
        for a in pva:
            a = a[3:-1]
            arr = next(arrays)
            arr[i] = [int(k) for k in a.split(',')]
        i += 1
        
dists = zeros(N, dtype = uint64)
amclos = zeros(N, dtype = uint64) #amount of time each particle has been closest
i = 0

while True:
    dists[:] = sum(abs(pos), axis=1)
    k = argmin(dists)
    amclos[k] += 1
    if i%1000 == 0:
        k = argmax(amclos) #closest particle for most amount of ticks
        print("Closest %03i, amount %5i, Time %i" % (k, amclos[k], i/1000))
        if i > 10000:
            print('Probably safe to ctrl-C now...')
        
    vel[:,:] += acc[:,:]
    pos[:,:] += vel[:,:]
    i += 1
