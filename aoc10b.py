lines = open("inp10.txt")

inst = lines.read().strip() #I don't want newlines etc
#print(inst)
inst = [ord(b) for b in inst] #ASCII value
inst = inst + [17,31,73,47,23]


lis = 2*list(range(256))

i = 0
skip = 0
for lest in 64*inst:
    length = int(lest)
    lis[i:i+length] = lis[i:i+length][::-1]
    lis[0:i] = lis[256:256+i]
    lis[256:]=lis[:256]
    i += length + skip
    i %= 256
    skip += 1
#print(lis[:256])
dense = []
for j in range(16):
    densi = 0
    for k in range(16):
        nu = lis[16*j + k]
        densi ^= nu
        #print(nu,densi)
    
    dense.append(hex(densi)[2:])
    if len(dense[-1]) == 1: #must be 2 digits
        dense[-1] = '0' + dense[-1]
    print(dense[-1])
print(''.join(dense))
