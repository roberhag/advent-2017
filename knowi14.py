"""
barn child trapp stairs steps trinn
"""
from numpy import *
steps = 30

wtg = zeros(steps, dtype=uint64)
wtg[0] = 1 #one way to get to first step
wtg[1] = 2 #two ways to get to second step
wtg[2] = 4 #four ways to get to third step (1-2-3, 1-3, 2-3, 3)
for i in range(3,steps):
    wtg[i] = wtg[i-1] + wtg[i-2] + wtg[i-3]

print(wtg[:5])
print(wtg[-1])
