from numpy import *
dirs = (array((1,0)),array((0,1)),array((-1,0)),array((0,-1)))
#matrix notation, first is row y, second is column x
di = 2

mappe = []
with open('inp22.txt', 'r') as infile:
    for line in infile:
        line = line.strip().replace('#', '2') #infected
        line = list(line.replace('.', '0'))   #clean
        mappe.append(array(line, dtype=uint8))
        
mappe = array(mappe, dtype=uint8)
mappe = pad(mappe, 500, 'constant', constant_values=0)

#print(mappe[0:10,0:10])
pos = array((len(mappe[0])//2 , len(mappe)//2 ),dtype=int)
bursti = 0

print(len(mappe[0]), len(mappe))

for i in range(10000000):
    inf = mappe[pos[0],pos[1]]
    if inf == 2:
        di -= 1 #right
    elif inf == 1:
        bursti += 1
    elif inf == 0:
        di += 1 #left
        #bursti += 1
    elif inf == 3: #flagged
        di += 2
    di %= 4
    inf += 1
    inf %= 4
    mappe[pos[0],pos[1]] = inf #swapped
    pos += dirs[di]
    if i % 100000 == 0:
        print(i/100000, pos)
print(bursti)
