from numpy import *

rules2 = []
rules3 = []

with open('inp21.txt', 'r') as infile:
    i = 0
    for line in infile:
        i += 1
        fro, to = line.strip().split(' => ')
        frule = []
        trule = []
        #print(fro.count('/') + 1, fro.count('#'), to.count('#'))
        for fl in fro.split('/'):
            frl = []
            for f in fl:
                if f=='#':
                    frl.append(1)
                else:
                    frl.append(0)
            frule.append(frl)
        for tl in to.split('/'):
            trl = []
            for t in tl:
                if t=='#':
                    trl.append(1)
                else:
                    trl.append(0)
            trule.append(trl)
        if len(frule) == 2:
            ru = rules2
        else:
            ru = rules3
        fr = array(frule, dtype=bool)
        rotflip = [fr, fliplr(fr), flipud(fr), fliplr(rot90(fr, 1)), rot90(fliplr(fr), 1),
                rot90(fr, 1), rot90(fr, 2), rot90(fr, 3), array(trule, dtype=bool)]
        if i == 63:
            print("The guy")
            for x in rotflip:
                print(x)
            print()
        ru.append(rotflip.copy())


#rules2 = asarray(rules2, dtype=bool)
#rules3 = asarray(rules3, dtype=bool)
suru2 = [sum(r[0]) for r in rules2]
#suru2 = argsort(sums)
#rules2 = rules2[argsort(sums)]
suru3 = [sum(r[0]) for r in rules3]
#suru3 = argsort(sums)
print(suru2)

imag = [[0,1,0],
[0,0,1],
[1,1,1]]
imag = array(imag, dtype=bool)

for i in range(18):
    si = len(imag)
    print(i)
    #print(imag, "IMAGE")
    if si%2 == 0:
        nimg = zeros((3*si//2, 3*si//2), dtype=bool)
        for j in range(si//2):
            for k in range(si//2):
                sqr = imag[j*2:(j+1)*2,k*2:(k+1)*2]
                susq = sum(sqr)
                #print(sqr, susq)
                ru2mask = suru2 == susq
                for ii in range(len(ru2mask)):
                    if not ru2mask[ii]: #not correct amount of colored pixels
                        continue
                    rule = rules2[ii]
                    outp = rule[-1]
                    for inp in rule[:-1]:
                        if ((inp == sqr).all()):
                            nimg[j*3:(j+1)*3,k*3:(k+1)*3] = outp
                            #print("rule2:", ii)
                            break
                    else:
                        continue
                    break
    elif si%3 == 0:
        nimg = zeros((4*si//3, 4*si//3), dtype=bool)
        for j in range(si//3):
            for k in range(si//3):
                sqr = imag[j*3:(j+1)*3,k*3:(k+1)*3]
                susq = sum(sqr)
                #print(sqr, susq)
                ru3mask = suru3 == susq
                
                for ii in range(len(ru3mask)):
                    if not ru3mask[ii]: #not correct amount of colored pixels
                        continue
                    rule = rules3[ii]
                    outp = rule[-1]
                    for inp in rule[:-1]:
                        if ((inp == sqr).all()):
                            nimg[j*4:(j+1)*4,k*4:(k+1)*4] = outp
                            #print("rule3:", ii)
                            break
                    else:
                        continue
                    break
                else:
                    print("no match?!")
    else:
        print ("something wrong")
        break
    imag = nimg

#print(array(imag, dtype=uint8))
print(sum(imag))
