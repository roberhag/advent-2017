from numpy import *
from sys import exit
#regs = {}

#with open("inp25.txt") as infile:
    #I think I will hardcode here            

Nsteps = 12481997
state = 1 # A=1, F=6
tape = zeros(Nsteps)
p = 0

for i in range(Nsteps):
    if i % 100000 == 0:
        print (i/100000, "/125")
    val = tape[p]
    if ((state in [1,4]) and val == 0) or (state in [2, 5, 6]):
        tape[p] = 1
    else:
        tape[p] = 0
        
    if (state == 1 and val == 0) or (state == 2 and val == 1) or (state in [4, 6]):
        p += 1
    else:
        p -= 1
    
    if (state in [2, 4] and val == 0) or (state == 6 and val == 1) :
        state = 1
    elif (state in [1, 3] and val == 0) or (state == 4 and val == 1):
        state = 2
    elif (state in [1, 5] and val == 1):
        state = 3
    elif (state == 6 and val == 0) or (state == 2 and val == 1):
        state = 4
    elif (state == 3 and val == 1):
        state = 5
    elif (state == 5 and val == 0):
        state = 6

print(sum(tape))

