from collections import defaultdict
depths = defaultdict(lambda:None)

with open("inp13.txt") as infile:
    for line in infile:
        p, d = line.split(': ')
        depths[int(p)] = int(d)

sever = 0
for i in range (max(depths.keys()) + 1): #i is our depth
    r = depths[i]
    if r: #there is a wall here
        p = i % (2*(r-1)) #time it takes to move back and forth
        if p == 0:
            #print('caught', i, r)
            sever += i*r
print(sever)
