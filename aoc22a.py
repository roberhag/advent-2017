from numpy import *
dirs = (array((1,0)),array((0,1)),array((-1,0)),array((0,-1)))
#matrix notation, first is row, second is column
di = 2

mappe = []
with open('inp22.txt', 'r') as infile:
    for line in infile:
        line = line.strip().replace('#', '1')
        line = list(line.replace('.', '0'))
        mappe.append(array(line, dtype=uint8))
        
mappe = array(mappe, dtype=uint8)
mappe = pad(mappe, 300, 'constant', constant_values=0)

#print(mappe[0:10,0:10])
pos = array((len(mappe[0])//2 , len(mappe)//2 ),dtype=int)
bursti = 0

print(len(mappe[0]), len(mappe))

for i in range(10000):
    inf = mappe[pos[0],pos[1]]
    if inf:
        di -= 1 #right
    else:
        di += 1 #left
        bursti += 1
    di %= 4
    mappe[pos[0],pos[1]] = 1-inf #swapped
    pos += dirs[di]
    if i % 100 == 0:
        print(pos)
print(bursti)
