from numpy import *

progs = "0123456789abcdef"
progs = [c for c in progs]
np = len(progs)

with open("inp16.txt") as infile:
    textfile = infile.read()
    #textfile = "s1,x3/4,pe/b"
    for line in textfile.split(','):
        c = line[0]
        if c == 's': #spin
            n = int(line[1:])
            progs2 = progs[-n:] + progs[:-n]
        else:
            progs2 = progs
            if c == 'x': #exchange
                a,b = [int(i) for i in line[1:].split('/')]
                pp = progs[b]
                progs2[b] = progs[a]
                progs2[a] = pp
            else: #Partner
                pass #happens even number of times
        progs = progs2
    
print(progs)
moveone = [int(c,base=16) for c in progs]
moveone = array(moveone, dtype=uint)
progs = "abcdefghijklmnop"
progs = [c for c in progs]


moveto = array(range(len(progs)), dtype=uint)

for ddd in range(1000000000):
    if ddd%1000000 == 0:
        print(ddd/1000000)

    moveto[:] = moveto[moveone]
print(''.join([progs[moveto[i]] for i in range(len(progs)) ] ))

