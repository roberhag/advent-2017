infi = open("inp12.txt")
lis = []
for line in infi:
    n, cons = line.split('<->')
    cons = cons.split(',')
    lislis = [int(con.strip()) for con in cons]
    lis.append(lislis)

visited = []
checknext =[0]

while len(checknext)>0:
    check = checknext.pop()
    visited.append(check)
    for l in lis[check]:
        if l not in visited + checknext:
            checknext.append(l)

print(len(visited))

