from numpy import *

afac = 16807
bfac = 48271

div = 2147483647

A = 883
B = 879

#A = 65
#B = 8921

#with open("inp15.txt") as infile:
#    for line in infile:
#        p, d = line.split(': ')

match = 0

AB = array((A,B), dtype=uint)
facs = array((afac,bfac), dtype=uint)
tests = zeros(2, dtype=uint)

for i in range(40000000):
    if i%100000 == 0: print(i/1e6)
    AB[:] = AB*facs%div
    tests[:] = AB%(65536)
    
    match += (tests[0] == tests[1])
    

print(match)
